

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegisterDao {
	
	private String dbUrl="jdbc:mysql://localhost:3306/userdb";
	private String dbUname="root";
	private String dbPassword="rootpasswordgiven";
	private String dbDriver="com.mysql.cj.jdbc.Driver";
	
	public void loadDriver(String dbDriver)
	{
		try
		{
			Class.forName(dbDriver);
		}catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public Connection getConnection()
	{
		Connection con = null;
		try {
			con = DriverManager.getConnection(dbUrl, dbUname, dbPassword);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return con;
	}
	
	
	
	
	public String insert(member object)
	{
		loadDriver(dbDriver);
		Connection con = getConnection();
		String result = "Data entered successfully";
		String sql = "insert into object values(?, ?, ?, ?, ?, ?)";
		
		PreparedStatement ps;
			
		try {
		ps = con.prepareStatement(sql);
		ps.setString(1, object.getFirstname());
		ps.setString(2, object.getLastname());
		ps.setString(3, object.getusername());
		ps.setString(3, object.getPassword());
		ps.setString(5, object.getAddress());
		ps.setString(6, object.getContact());
		
		ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
			result= "Data not entered";
		}
		return result;
	}

}
